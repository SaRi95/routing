import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  age: string;

  constructor(public router: Router) {}

  show_result() {
    this.router.navigateByUrl('result/' + this.age.toString());
  }

}
